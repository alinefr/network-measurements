FROM python:3.7-slim as base

ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONFAULTHANDLER 1

FROM base AS python-deps

RUN pip install pipenv

COPY Pipfile .
COPY Pipfile.lock .
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --deploy

FROM base AS runtime

COPY --from=python-deps /.venv /.venv
ENV PATH="/.venv/bin:$PATH"

WORKDIR /network-measurements

COPY . .

RUN apt-get update && apt-get install --no-install-recommends -y cron gnupg1 apt-transport-https dirmngr software-properties-common
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 379CE192D401AB61
RUN add-apt-repository "deb https://ookla.bintray.com/debian buster main"
RUN apt-get update && apt-get install speedtest
RUN crontab /network-measurements/cronfile

CMD ["/network-measurements/docker-entrypoint.sh"]
