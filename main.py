import json
import os
import re
import subprocess

from influxdb import InfluxDBClient
from os.path import join, dirname

try:
    from dotenv import load_dotenv
    dotenv_path = join(dirname(__file__), '.env')
    load_dotenv(dotenv_path)
except:
    pass

try:
    influx_db_host = os.environ['INFLUX_DB_HOST']
    influx_db_name = os.environ['INFLUX_DB_NAME']
    influx_db_user = os.environ['INFLUX_DB_USER']
    influx_db_password = os.environ['INFLUX_DB_PASSWORD']
    speedtest_server_id = os.environ['SPEEDTEST_SERVER_ID']
except KeyError as e:
    raise Exception('Environment variable {} not set'.format(e.args[0]))

influx_db_port = os.environ.get('INFLUX_DB_PORT', 8086)

res = subprocess.Popen(f'speedtest \
                       -f json \
                       -s {speedtest_server_id} \
                       --accept-license \
                       --accept-gdpr', shell=True, stdout=subprocess.PIPE).stdout.read().decode('utf-8')

speedtest = json.loads(res)
download = speedtest['download']['bandwidth'] / 125000
upload = speedtest['upload']['bandwidth'] / 125000
ping = speedtest['ping']['latency'] / 125000

speed_data = [
    {
        "measurement" : "internet_speed",
        "tags" : {
            "host": "cabuloso.home",
            "server_id": speedtest_server_id
        },
        "fields" : {
            "download": download,
            "upload": upload,
            "ping": ping
        }
    }
]
client = InfluxDBClient(influx_db_host, influx_db_port, influx_db_user, influx_db_password, influx_db_name)

client.write_points(speed_data)
